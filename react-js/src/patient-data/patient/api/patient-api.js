import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    get_patients: '/patient/',
    post_patient: "/patient/",
    get_patient_by_userid: "/patient/byuserid/"
};

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patients, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patients + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientWithUserid(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patient_by_userid + params, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPatient(patient, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_patient, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deletePatient(patient, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_patient, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updatePatient(patient, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_patient, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getPatients,
    getPatientById,
    getPatientWithUserid,
    postPatient,
    deletePatient,
    updatePatient
};
