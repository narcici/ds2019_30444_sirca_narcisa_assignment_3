import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import PatientForm from "./patient-form";
import UserPatientForm from "../../user/user-patient-form";
import UpdatePatientForm from "./update-patient-form";
import * as API_PATIENT from "./api/patient-api";
import * as API_USER from "../../user/api/user-api";

class Patients extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null,

            user: null,
            seeUpdatePatientForm: false, //boolean to show update patient form
            selectedPatient: null     //row selected for updating
        };

        this.tableData = [];

        this.columns = [
            {
                Header: 'Name',
                accessor: 'name',
            },
            {
                Header: "Gender",
                accessor: "gender",
            },
            {
                Header: "Birthdate",
                accessor: "birthdate",
            },
            {
                Header: "Address",
                accessor: "address",
            },
            {
                Header: "Medical Record",
                accessor: "medicalrecord",
            },
            {
                Header: 'Update',
                Cell: row => (
                    <div>
                        <button onClick={() => {
                            this.handleUpdate(this.tableData, row.index)
                        }}>Update
                        </button>
                    </div>
                )
            },
            {
                Header: 'Delete',
                Cell: row => (
                    <div>
                        <button onClick={() => {
                            this.handleDelete(this.tableData, row.index)
                        }}>Delete
                        </button>
                    </div>
                )
            }

        ];

        this.setUser = this.setUser.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.doTheUpdate = this.doTheUpdate.bind(this);

    }

    setUser = (id) => {
        console.log("setuser: " + id);

        API_USER.getUserById( id, (result, status, err) => {
            this.setState({user: result}, () => { console.log("setState user", this.state)});
        });
    }

    handleDelete(table, index) {
        console.log("delete " + index.toString()); ///all good
        let deletedPatient = table[index];
        API_PATIENT.deletePatient(deletedPatient, () => console.log("deleted"));
        this.refresh();
    }

    handleUpdate(table, index) {
        console.log("tabel[index] : ", table[index]);
        this.setState({selectedPatient: table[index]}, function() { console.log("setState completed", this.state) });
        this.setState({seeUpdatePatientForm: true}); ///porneste form
    }

    doTheUpdate(newPatient) //partial update data for tha patient
    {
        let updatedPatient = this.state.selectedPatient;
        updatedPatient.name = newPatient.name;
        updatedPatient.address = newPatient.address;
        updatedPatient.medicalrecord = newPatient.medicalrecord;

        return API_PATIENT.updatePatient(updatedPatient, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated patient with id: " + result);
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
            this.setState({seeUpdatePatientForm: false}); ///reseteaza sa dispara formul
        });
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchPatients();
    }


    fetchPatients() {
        return API_PATIENT.getPatients((result, status, err) => {
            console.log(result);
            if (result !== null && status === 200) {
                result.forEach(x => {
                    this.tableData.push({
                        user: x.user,
                        patientId: x.patientId,
                        name: x.name,
                        gender: x.gender,
                        birthdate: x.birthdate,
                        address: x.address,
                        medicalrecord: x.medicalrecord,
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh() {
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={this.columns}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>
                {this.state.seeUpdatePatientForm &&
                <Row>
                    <Col>
                        <Card body>
                            <UpdatePatientForm update={this.doTheUpdate}/>
                        </Card>
                    </Col>
                </Row>
                }
                <Row>
                    <Col>
                        <Card body>
                            {this.state.user !== null &&
                            <div>
                                <Row>
                                <Col>
                                    <p>{this.state.user.username}  </p>
                                </Col>
                            </Row>
                                <Row>
                                    <Col>
                                        <p>{this.state.user.password} </p>
                                    </Col>
                                </Row>
                            </div> }
                            <div>
                                <PatientForm registerPatient={this.refresh}>

                                </PatientForm>
                            </div>
                        </Card>
                    </Col>
                    <Col>
                        <Card body>
                            <div>
                                <UserPatientForm registerUser={this.refresh} changeUser={this.setUser}>

                                </UserPatientForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Patients;
