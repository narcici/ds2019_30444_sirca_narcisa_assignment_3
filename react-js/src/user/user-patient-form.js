import React from 'react';
import validate from "./validators/user-validators";
import TextInput from "./fields/UserTextInput";
import '../person-data/person/fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/user-api";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";


class UserPatientForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {

                username: {
                    value: '',
                    placeholder: 'username...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                password: {
                    value: '',
                    placeholder: '1234...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        console.log("Element: " + name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerUser(user) {
        return API_USERS.postUser(user, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted user from patient with id: " + result);
                localStorage.setItem("lastInsertedUserId", result);
                console.log("insertedUserId: " + localStorage.getItem("lastInsertedUserId"));
                localStorage.setItem("isPatientUserInserted", "true");

                this.props.changeUser(result); //userid
            } else {
                this.state.errorStatus = status;
                this.error = error;
                //this.state.insertedUserId = 0;
                localStorage.setItem("lastInsertedUserId", "0");
                localStorage.setItem("isPatientUserInserted", "false");
            }
        });
    }

    handleSubmit = function (e) {

        console.log("New user data:");
        console.log("Username: " + this.state.formControls.username.value);
        console.log("Role: 1");

        let user = {
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value,
            role: 1,
        };

        this.registerUser(user);

        e.preventDefault();
    }

    render() {
        return (

            <form onSubmit={this.handleSubmit}>

                <h1>First, registrate an user for the new patient</h1>

                <p> Username: </p>

                <TextInput name="username"
                           placeholder={this.state.formControls.username.placeholder}
                           value={this.state.formControls.username.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.username.touched}
                           valid={this.state.formControls.username.valid}
                />
                {this.state.formControls.username.touched && !this.state.formControls.username.valid &&
                <div className={"error-message row"}> * Username must have at least 3 characters </div>}

                <p> Password: </p>
                <TextInput name="password"
                           placeholder={this.state.formControls.password.placeholder}
                           value={this.state.formControls.password.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.password.touched}
                           valid={this.state.formControls.password.valid}
                />

                {this.state.formControls.password.touched && !this.state.formControls.password.valid &&
                <div className={"error-message"}> * Password must have at least 3 characters</div>}

                <p></p>
                <Button variant="success"
                        type={"submit"}
                        disabled={!this.state.formIsValid}>
                    Submit
                </Button>
                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </form>

        );
    }
}

export default UserPatientForm;
