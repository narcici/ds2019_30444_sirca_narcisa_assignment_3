import React from 'react'
import * as API_CAREGIVER from "../caregiver-data/caregiver/api/caregiver-api";

import {Card, Col, Row} from 'reactstrap';
import Table from "../commons/tables/table";
import WebSocketListener from "../ws/WebSocketListener";
//import  { useState } from 'react';


import { EventEmitter } from "events";


// const Many = require('extends-classes')

const listener = new WebSocketListener();

// const [notification, setNotification] = useState("");
//class CaregiverRole extends Many(React.Component, EventEmitter) {
//class CaregiverRole extends React.Component, EventEmitter {

class CaregiverRole extends React.Component {

    constructor() {
        super();

        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            currentCaregiver: "",

            socket_activities: []
        };

        this.columns = [
            {
                Header: 'Patient Name',
                accessor: 'name',
            },
            {
                Header: "Gender",
                accessor: "gender",
            },
            {
                Header: "Birthdate",
                accessor: "birthdate",
            },
            {
                Header: "Address",
                accessor: "address",
            },
            {
                Header: "Medical Record",
                accessor: "medicalrecord",
            }
        ];

        this.fetchThisCaregiver = this.fetchThisCaregiver.bind(this);
        listener.on("event", event => {
            if (event.eventType === "ANOMALOUS_ACTIVITY") {
                this.activityEventHandler(event.activity);
                this.forceUpdate();
                // setNotification(notification + event.activity);
            }
        });

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchThisCaregiver();
    }

    fetchThisCaregiver = () => {
        let userid = localStorage.getItem("currentUserId"); //returneaza string

        console.log("passed usr id " + parseInt(userid));

        API_CAREGIVER.getCaregiverByUserid(parseInt(userid), (result, status, error) => {
            console.log(result);
            if (result !== null && (status === 200 || status === 201)) {
                this.setState({currentCaregiver: result}, function () {
                    console.log("caregiver set");
                });

            } else {
                console.log("caregiver role error");
            }

            this.forceUpdate();

        });

    }

    activityEventHandler(activity) {
        this.state.socket_activities = this.state.socket_activities.concat([activity]); //nush

        console.log("anomalous", activity);
        alert("Anomalous activity detected!");
        ///this.emit("change", this.state);
    }
    refresh() {
        this.forceUpdate()
    }


    render() {

        return (

            <div>

                {/*{*/}
                {/*    notification*/}
                {/*}*/}

                <Row>
                    <Col>
                        <Card body>
                            <h3>Caregiver Personal Information</h3>
                            <p></p>
                            <h4>Name: {this.state.currentCaregiver.name}</h4>
                            <p></p>
                            <h5>Address: {this.state.currentCaregiver.birthdate}</h5>
                            <h5>Birth date: {this.state.currentCaregiver.address}</h5>
                            <h5>Gender: {this.state.currentCaregiver.gender}</h5>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Table
                            data={this.state.currentCaregiver.patientDTOList }
                            columns={this.columns}
                            pageSize={10}
                        />
                    </Col>
                </Row>
            </div>
        );

    };

}


export default CaregiverRole;

const c = new CaregiverRole();



