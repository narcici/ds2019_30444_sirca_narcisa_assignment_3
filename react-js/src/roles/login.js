import React from 'react';
import validate from "../user/validators/user-validators";
import '../user/fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "../user/api/user-api";
import UserTextInput from "../user/fields/UserTextInput";


import {Route, withRouter} from 'react-router-dom'


class Login extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            isLoaded:false,
            role:5,

            formIsValid: false,

            formControls: {

                username: {
                    value: '',
                    placeholder: 'username...',
                },
                password: {
                    value: '',
                    placeholder: '',
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);


        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    login(username) {
        return API_USERS.getUserByUsername(username, (result, status, error) => {
            console.log(result);

            if (result !== null && status === 200) {

                localStorage.setItem("loggedinUser", result); //maybe i can use it??
                localStorage.setItem("currentUserId", result.id);
                localStorage.setItem("currentUserRole", result.role);

                this.setState({role: result.role});
                this.setState({isLoaded: true});

                console.log("current user id:" + localStorage.getItem("currentUserId"));

                console.log("current user id:" + localStorage.getItem("currentUserId"));
                console.log("current role:" + localStorage.getItem("currentUserRole"));
                ///  this.props.refresh();
            } else {
                localStorage.setItem("loggedinUser", "");
                localStorage.setItem("currentUserId", "-1");
                localStorage.setItem("currentUserRole", "-1");
                console.log("current user id:" + localStorage.getItem("currentUserId"));
                console.log("current role:" + localStorage.getItem("currentUserRole"));
                this.state.errorStatus = status;
                this.error = error;
            }
            console.log("login final "+ localStorage.getItem("currentUserId") +" " + localStorage.getItem("currentUserRole") );

              if(localStorage.getItem("currentUserRole") === "0" ) //doctor role
                  this.props.history.push("/doc");

             else if (localStorage.getItem("currentUserRole") === "1" ) //patient role
                  this.props.history.push("/pat");

              else if (localStorage.getItem("currentUserRole") === "2" ) //caregiver role
                  this.props.history.push("/care");
              else
                  this.props.history.push("/");

            //this.props.history.push("/doc");

        });
    }

    handleSubmit() {

        let username = this.state.formControls.username.value;
        this.login(username);

    }

    render() {
        return (

            <div>
                <form>

                    <h1>Login</h1>

                    <p> Username: </p>

                    <UserTextInput name="username"
                                   placeholder={this.state.formControls.username.placeholder}
                                   value={this.state.formControls.username.value}
                                   onChange={this.handleChange}
                                   touched={this.state.formControls.username.touched}
                                   valid={this.state.formControls.username.valid}
                    />

                    <p> Password: </p>
                    <UserTextInput name="password"
                                   placeholder={this.state.formControls.password.placeholder}
                                   value={this.state.formControls.password.value}
                                   onChange={this.handleChange}
                                   touched={this.state.formControls.password.touched}
                                   valid={this.state.formControls.password.valid}
                    />

                    <p></p>
                </form>
                <Button variant="success"
                        type={"submit"}
                        onClick={() => {
                            this.handleSubmit()
                        }}
                        // disabled={!this.state.formIsValid}
                >
                    Submit
                </Button>

            </div>

        );
    }
}

export default withRouter(Login);