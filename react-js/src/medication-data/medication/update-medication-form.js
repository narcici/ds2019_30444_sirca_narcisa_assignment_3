import React from 'react';
import validate from "./validators/medication-validators";
import TextInput from "../../commons/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";

class UpdateMedicationForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                name: {
                    value: '',
                    placeholder: 'Another Name...',
                },
                dosage: {
                    value: '',
                },

                sideeffects: {
                    value: '',
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " + name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    handleUpdate = ( ) => {
        let newMedicationData = {
            name: this.state.formControls.name.value,
            dosage: this.state.formControls.dosage.value,
            sideeffects: this.state.formControls.sideeffects.value
        };
        console.log(" deceee", newMedicationData);
        this.props.update(newMedicationData);  //call a props method
    }

    render() {
        return (
            <div>
                <form>

                    <h1>Update data for this medciation:</h1>

                    <p> Name: </p>

                    <TextInput name="name"
                               placeholder={this.state.formControls.name.placeholder}
                               value={this.state.formControls.name.value}
                               onChange={this.handleChange}
                    />

                    <p> New Dosage: </p>
                    <TextInput name="dosage"
                                      placeholder={this.state.formControls.dosage.placeholder}
                                      value={this.state.formControls.dosage.value}
                                      onChange={this.handleChange}
                                      touched={this.state.formControls.dosage.touched}
                                      valid={this.state.formControls.dosage.valid}
                    />

                    <p> New Side Effects: </p>
                    <TextInput name="sideeffects"
                                      placeholder={this.state.formControls.sideeffects.placeholder}
                                      value={this.state.formControls.sideeffects.value}
                                      onChange={this.handleChange}
                                      touched={this.state.formControls.sideeffects.touched}
                                      valid={this.state.formControls.sideeffects.valid}
                    />

                    <p></p>

                    {this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
                </form>

                <Button onClick= {() => {
                    this.handleUpdate()
                }}
                        disabled={!this.state.formIsValid}>
                    Save the changes
                </Button>
            </div>


        );
    }
}

export default UpdateMedicationForm;