import React from 'react';
import TextInput from "../../commons/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_MEDICATION from "./api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import validate from "./validators/medication-validators";


class MedicationForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {

                name: {
                    value: '',
                    placeholder: 'Medication Name...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                dosage: {
                    value: '',
                    valid: false,
                    touched: false,
                    validationRules: {
                        maxLength: 10,
                    }
                },

                sideeffects: {
                    value: '',
                    valid: false,
                    touched: false
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.registerMedication = this.registerMedication.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " + name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerMedication(m) {
        return API_MEDICATION.postMedication(m, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted medication with id: " + result);
               // this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }


    handleSubmit() {

        console.log("New medication data:");
        console.log("Name: " + this.state.formControls.name.value);
        console.log("dosage: " + this.state.formControls.dosage.value);
        console.log("side effects: " + this.state.formControls.sideeffects.value);

        let medication = {
            name: this.state.formControls.name.value,
            dosage: this.state.formControls.dosage.value,
            sideeffects: this.state.formControls.sideeffects.value
        };

        if(medication.name !== "" && medication.dosage !== "" && medication.sideeffects !== "") {
            this.registerMedication(medication);
        }
    }

    render() {
        return (
            <div>
                <form>

                    <h1>Insert new mediation</h1>

                    <p> Name: </p>
                    <TextInput name="name"
                               placeholder={this.state.formControls.name.placeholder}
                               value={this.state.formControls.name.value}
                               onChange={this.handleChange}
                               touched={this.state.formControls.name.touched}
                               valid={this.state.formControls.name.valid}
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                    <p> Dosage: </p>
                    <TextInput name="dosage"
                               value={this.state.formControls.dosage.value}
                               onChange={this.handleChange}
                               touched={this.state.formControls.dosage.touched}
                               valid={this.state.formControls.dosage.valid}
                    />
                    {this.state.formControls.dosage.touched && !this.state.formControls.dosage.valid &&
                    <div className={"error-message row"}> * dosage must have at must 10 characters </div>}


                    <p> Side effects: </p>
                    <TextInput name="sideeffects"
                               value={this.state.formControls.sideeffects.value}
                               onChange={this.handleChange}
                               touched={this.state.formControls.sideeffects.touched}
                               valid={this.state.formControls.sideeffects.valid}
                    />

                    <p></p>

                    {this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

                </form>
                <Button variant="success"
                        onClick={() => {
                            this.handleSubmit()
                        }}
                        disabled={!this.state.formIsValid}>
                    Add Medication
                </Button>
            </div>
        );
    }
}

export default MedicationForm;