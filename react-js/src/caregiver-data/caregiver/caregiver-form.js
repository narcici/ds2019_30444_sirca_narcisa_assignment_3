import React from 'react';
import validate from "./validators/caregiver-validators";
import CaregiverTextInput from "./fields/CaregiverTextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_CAREGIVER from "./api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import * as API_USERS from "../../user/api/user-api";


class CaregiverForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            user: null,

            formControls: {

                name: {
                    value: '',
                    placeholder: 'Full Name...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                gender: {
                    value: '',
                    placeholder: 'M/F',
                    valid: false,
                    touched: false,
                    validationRules: {
                        maxLength: 1,
                        isRequired: true
                    }
                },

                birthdate: {
                    value: '',
                    placeholder: 'dd/mm/yyyy',
                    valid: false,
                    touched: false,

                },
                address: {
                    value: '',
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    valid: false,
                    touched: false,

                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.registerCaregiver = this.registerCaregiver.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " + name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerCaregiver(caregiver) {
        return API_CAREGIVER.postCaregiver(caregiver, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted caregiver with id: " + result);
                //this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }


    handleSubmit() {

        console.log("New caregiver data:");
        console.log("Name: " + this.state.formControls.name.value);
        console.log("Gender: " + this.state.formControls.gender.value);
        console.log("Birthdate: " + this.state.formControls.birthdate.value);
        console.log("Address: " + this.state.formControls.address.value);

        API_USERS.getUserById(parseInt(localStorage.getItem("lastInsertedUserId"), 10),
            (result, status, error) => {

                this.setState({...this.state, user: result}, () => console.log("user setat", result));
                console.log("User din api call: ", {...this.state.user});

                let caregiver = {
                    user: result,
                    name: this.state.formControls.name.value,
                    birthdate: this.state.formControls.birthdate.value,
                    address: this.state.formControls.address.value,
                    gender: this.state.formControls.gender.value,
                };

                if (caregiver.user !== null) {
                    this.registerCaregiver(caregiver);
                }
            });
    }

    render() {
        return (
            <div>
                <form>

                    <h1>Insert new caregiver</h1>

                    <p> Name: </p>

                    <CaregiverTextInput name="name"
                                        placeholder={this.state.formControls.name.placeholder}
                                        value={this.state.formControls.name.value}
                                        onChange={this.handleChange}
                                        touched={this.state.formControls.name.touched}
                                        valid={this.state.formControls.name.valid}
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                    <p> Gender: </p>
                    <CaregiverTextInput name="gender"
                                        placeholder={this.state.formControls.gender.placeholder}
                                        value={this.state.formControls.gender.value}
                                        onChange={this.handleChange}
                                        touched={this.state.formControls.gender.touched}
                                        valid={this.state.formControls.gender.valid}
                    />
                    {this.state.formControls.gender.touched && !this.state.formControls.gender.valid &&
                    <div className={"error-message"}> * Gender must be 1 character only: M or F!</div>}


                    <p> Birthdate: </p>
                    <CaregiverTextInput name="birthdate"
                                        placeholder={this.state.formControls.birthdate.placeholder}
                                        value={this.state.formControls.birthdate.value}
                                        onChange={this.handleChange}
                                        touched={this.state.formControls.birthdate.touched}
                                        valid={this.state.formControls.birthdate.valid}
                    />

                    <p> Address: </p>
                    <CaregiverTextInput name="address"
                                        placeholder={this.state.formControls.address.placeholder}
                                        value={this.state.formControls.address.value}
                                        onChange={this.handleChange}
                                        touched={this.state.formControls.address.touched}
                                        valid={this.state.formControls.address.valid}
                    />


                    <p></p>
                    {this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
                </form>
                <Button onClick={() => {
                    this.handleSubmit()
                }}
                    variant="success"
                        type={"submit"}
                        disabled={!this.state.formIsValid}>
                    Add caregiver
                </Button>
            </div>

        );
    }
}

export default CaregiverForm;
