import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    get_caregivers: '/caregiver/',
    post_caregiver: "/caregiver/",
    get_caregiver_by_userid: "/caregiver/byuserid/"
};

function getCaregivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_caregivers, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCaregiverById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_caregivers + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCaregiverByUserid(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_caregiver_by_userid + params, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postCaregiver(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_caregiver, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteCaregiver(caregiver, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_caregiver, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateCaregiver(caregiver, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_caregiver, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export {
    getCaregivers,
    getCaregiverById,
    postCaregiver,
    deleteCaregiver,
    updateCaregiver,
    getCaregiverByUserid
};
