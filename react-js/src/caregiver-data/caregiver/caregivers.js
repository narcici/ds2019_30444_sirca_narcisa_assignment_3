import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import CaregiverForm from "./caregiver-form";
import UserCaregiverForm from "../../user/user-caregiver-form";
import * as API_USER from "../../user/api/user-api";
import * as API_CAREGIVER from "./api/caregiver-api"
import UpdateCaregiverForm from "./update-caregiver-form";

class Caregivers extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null,

            user:null,
            seeUpdateCaregiverForm: false, //boolean to show form
            selectedCaregiver: null     //row selected for updating

        };

        this.columns = [
            {
                Header: 'Name',
                accessor: 'name',
            },
            {
                Header: "Gender",
                accessor: "gender",
            },
            {
                Header: "Birthdate",
                accessor: "birthdate",
            },
            {
                Header: "Address",
                accessor: "address",
            },
            {
                Header: 'Update',
                Cell: row => (
                    <div>
                        <button onClick={() => {
                            this.handleUpdate(this.tableData, row.index)
                        }}>Update
                        </button>
                    </div>
                )
            },
            {
                Header: 'Delete',
                Cell: row => (
                    <div>
                        <button onClick={() => {
                            this.handleDelete(this.tableData, row.index)
                        }}>Delete
                        </button>
                    </div>
                )
            }
        ];

        this.tableData = [];

        this.setUser = this.setUser.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.doTheUpdate = this.doTheUpdate.bind(this);

    }

    setUser = (id) => {
        console.log("setuser: " + id);

        API_USER.getUserById( id, (result, status, err) => {
            this.setState({user: result}, () => { console.log("setState user", this.state)});
        });
    }

    handleDelete(table, index) {
        console.log("delete " + index.toString()); ///all good
        let deletedCaregiver = table[index];
        API_CAREGIVER.deleteCaregiver(deletedCaregiver, () => console.log("deleted"));
        this.refresh();
    }

    handleUpdate(table, index) {
        console.log("tabel[index] : ", table[index]);
        this.setState({selectedCaregiver: table[index]}, function() { console.log("setState completed", this.state) });
        this.setState({seeUpdateCaregiverForm: true}); ///porneste form
    }

    doTheUpdate(newCaregiver) //partial update data for tha patient
    {
        let updatedCaregiver = this.state.selectedCaregiver;
        updatedCaregiver.name = newCaregiver.name;
        updatedCaregiver.address = newCaregiver.address;

        return API_CAREGIVER.updateCaregiver(updatedCaregiver, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated caregiver with id: " + result);
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
            this.setState({seeUpdateCaregiverForm: false}); ///reseteaza sa dispara formul
        });
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchCaregivers();
    }


    fetchCaregivers() {
        return API_CAREGIVER.getCaregivers((result, status, err) => {
            console.log(result);
            if (result !== null && status === 200) {
                result.forEach(x => {
                    this.tableData.push({
                        user: x.user,
                        caregiverId: x.caregiverId,
                        name: x.name,
                        birthdate: x.birthdate,
                        address: x.address,
                        gender: x.gender,
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh() {
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={this.columns}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                {this.state.seeUpdateCaregiverForm &&
                <Row>
                    <Col>
                        <Card body>
                            <UpdateCaregiverForm update={this.doTheUpdate}/>
                        </Card>
                    </Col>
                </Row>
                }

                <Row>
                    <Col>
                        <Card body>

                            {this.state.user !== null &&
                            <div>
                                <Row>
                                    <Col>
                                        <p>{this.state.user.username}  </p>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <p>{this.state.user.password} </p>
                                    </Col>
                                </Row>
                            </div> }

                            <div>
                                <CaregiverForm registerCaregiver={this.refresh} updatePatientChild={this.updatePatient}>

                                </CaregiverForm>
                            </div>
                        </Card>
                    </Col>
                    <Col>
                        <Card body>
                            <div>
                                <UserCaregiverForm registerUser={this.refresh} changeUser={this.setUser}>

                                </UserCaregiverForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Caregivers;
