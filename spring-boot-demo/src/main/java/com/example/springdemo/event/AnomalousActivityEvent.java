package com.example.springdemo.event;

import com.example.springdemo.dto.ActivityDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AnomalousActivityEvent extends BaseEvent {
    private final ActivityDTO activityDTO;

    public AnomalousActivityEvent(ActivityDTO activityDTO) {
        super(EventType.ANOMALOUS_ACTIVITY);
        this.activityDTO = activityDTO;
    }
}