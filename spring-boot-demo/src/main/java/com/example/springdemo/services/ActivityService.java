package com.example.springdemo.services;

import com.example.springdemo.SpringDemoApplication;
import com.example.springdemo.dto.ActivityDTO;
import com.example.springdemo.dto.builders.ActivityBuilder;
import com.example.springdemo.entities.Activity;
import com.example.springdemo.event.AnomalousActivityEvent;
import com.example.springdemo.repositories.ActivityRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

@Service
@RequiredArgsConstructor
public class ActivityService {

    private static final Logger log = LoggerFactory.getLogger(ActivityService.class);
    private final ActivityRepository activityREpository;
    private final ApplicationEventPublisher eventPublisher;

//    @Autowired
//    public ActivityService(ActivityRepository activityREpository) {
//        this.activityREpository = activityREpository;
//    }

//    @RabbitListener(queues = SpringDemoApplication.QUEUE_GENERIC_NAME)
//    public void receiveMessage(final Message message) {
//        log.info("Demo:::Received message as generic: {}", message.toString());
//    }
    /*
    trying to receive a generic message and to parse it as an actvity here, not ok
     */
    //@RabbitListener(queues = SpringDemoApplication.QUEUE_GENERIC_NAME)
    //public void receiveMessage(final Message message) {
    //    ObjectMapper mapper = new ObjectMapper();
    //    try {
    //        JsonNode node = mapper.readTree(message.getBody());
    //        JsonNode activityJson = node.get("activity");
    //        log.info("message.activity "+ activityJson);
    //        int id = activityJson.get("id").asInt();
    //        String activityName = activityJson.get("activity").asText();
    //        long s = activityJson.get("start").asLong();
    //        Date start= new Date(s);
    //        long e = activityJson.get("end").asLong();
    //        Date end= new Date(e);
    //        Activity activity= new Activity(id,activityName, start,end );
    //        log.info("Demo:::converted to Activity::", activity.toString());
    //        activityREpository.save(activity);
    //        //amu test pe Activity daca ii anomalous
    //        //daca ii ce se intampla??
    //        ActivityDTO activityDTO = ActivityBuilder.generateDTOFromEntity(activity);
    //        eventPublisher.publishEvent(new AnomalousActivityEvent(activityDTO));
    //
    //    }catch(Exception e){
    //        e.printStackTrace();
    //    }
    //}

    @RabbitListener(queues = SpringDemoApplication.QUEUE_GENERIC_NAME)
    public void receiveMessage(final Activity activity) {

        log.info("Demo:::Received message as specific class: {}", activity.toString());
        activityREpository.save(activity);

        long s = activity.getStart().getTime();
        long e = activity.getEnd().getTime();
        long time = e - s;

        boolean isAnomalousActivity = false;
        if(activity.getActivity().contentEquals("Sleeping")){
            if (time > 12 * 3600000) /// 12 hours
                isAnomalousActivity =true;
        }
        if(activity.getActivity().contentEquals("Leaving")){
            if (time > 12 * 3600000) /// 12 hours
                isAnomalousActivity =true;
        }
        if(activity.getActivity().contentEquals("Toileting")){
            if (time > 3600000) /// 1 hour
                isAnomalousActivity =true;
        }
        // test pe Activity daca ii anomalous
        if(isAnomalousActivity) {
            log.info("anomalus detected: " + time);
            ActivityDTO activityDTO = ActivityBuilder.generateDTOFromEntity(activity);
            eventPublisher.publishEvent(new AnomalousActivityEvent(activityDTO));
            //eventPublisher.publishEvent("anomalous to front");
        }
        isAnomalousActivity = false; //reset
    }
}