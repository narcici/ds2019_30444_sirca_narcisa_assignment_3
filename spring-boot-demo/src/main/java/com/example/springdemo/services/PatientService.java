package com.example.springdemo.services;

import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.PatientWithPlansDTO;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.dto.builders.PatientWithPlansBuilder;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    public final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public PatientWithPlansDTO findPatientById(Integer id){
        Optional<Patient> patient  = patientRepository.findById(id);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "user id", id);
        }
        return PatientWithPlansBuilder.generateDTOFromEntity(patient.get(), patient.get().getMedicationPlanList());
    }

    public List<PatientDTO> findAll(){
        List<Patient> persons = patientRepository.getAllOrdered();

        return persons.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public List<PatientWithPlansDTO> findAllFetch(){
        List<Patient> patientList = patientRepository.getAllFetch();

        return patientList.stream()
                .map(x-> PatientWithPlansBuilder.generateDTOFromEntity(x, x.getMedicationPlanList()))
                .collect(Collectors.toList());
    }

    public PatientWithPlansDTO findPatientByUserid(Integer userid){

        List<PatientWithPlansDTO> allpatients = findAllFetch();
        System.out.println("allfetch  " + allpatients.get(0).getName());
        List<PatientWithPlansDTO> result = allpatients.stream()
                .filter(c -> c.getUser().getId() == userid)
                .collect(Collectors.toList());

        PatientWithPlansDTO p = result.get(0);
        return p;
    }

    public Integer insert(PatientDTO patientDTO) {

        return patientRepository
                .save(PatientBuilder.generateEntityFromDTO(patientDTO))
                .getPatientid();
    }

    public Integer update(PatientDTO patientDTO) {

        Optional<Patient> person = patientRepository.findById(patientDTO.getPatientId());

        if(!person.isPresent()){
            throw new ResourceNotFoundException("Patient", "patient id", patientDTO.getPatientId().toString());
        }

        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getPatientid();
    }

    public Integer delete(PatientDTO patientDTO){
        this.patientRepository.deleteById(patientDTO.getPatientId());
        return 0;
    }

}