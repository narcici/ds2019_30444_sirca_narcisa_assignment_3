package com.example.springdemo.services;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.CaregiverWithPatientsDTO;
import com.example.springdemo.dto.builders.CaregiverBuilder;
import com.example.springdemo.dto.builders.CaregiverWithPatientsBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.CaregiverRepository;
import com.example.springdemo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private final CaregiverRepository caregiverRepository;
    private final UserRepository userRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository, UserRepository userRepository) {
        this.caregiverRepository = caregiverRepository;
        this.userRepository = userRepository;
    }

    public CaregiverDTO findCaregiverById(Integer id){
        Optional<Caregiver> caregiver  = caregiverRepository.findById(id);

        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "caregiver id", id);
        }
        return CaregiverBuilder.generateDTOFromEntity(caregiver.get());
    }

    public List<CaregiverDTO> findAll(){
        List<Caregiver> caregivers = caregiverRepository.findAll();

        return caregivers.stream()
                .map(x->CaregiverBuilder.generateDTOFromEntity(x))
                .collect(Collectors.toList());
    }

    public List<CaregiverWithPatientsDTO> findAllFetch(){
        List<Caregiver> personList = caregiverRepository.getAllFetch();

        return personList.stream()
                .map(x-> CaregiverWithPatientsBuilder.generateDTOFromEntity(x, x.getPatients()))
                .collect(Collectors.toList());
    }

    public Integer insert(CaregiverDTO caregiverDTO) {

        return caregiverRepository
                .save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO))
                .getCaregiverId();
    }

    public Integer update(CaregiverDTO caregiverDTO) {

        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverDTO.getCaregiverId());

        if(!caregiver.isPresent()){
            throw new ResourceNotFoundException("Caregiver", "caregiver id", caregiverDTO.getCaregiverId().toString());
        }

        return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getCaregiverId();
    }

    public Integer delete(CaregiverDTO caregiverDTO){
        this.caregiverRepository.deleteById(caregiverDTO.getCaregiverId());
        return 0;
    }

    public CaregiverWithPatientsDTO findCaregiverByUserid(Integer userid){
     Caregiver caregiver = caregiverRepository.getAllFetchUserid(userid);

        //CaregiverWithPatientsDTO caregiver = result.get(0);
        return CaregiverWithPatientsBuilder.generateDTOFromEntity(caregiver, caregiver.getPatients());
    }

}