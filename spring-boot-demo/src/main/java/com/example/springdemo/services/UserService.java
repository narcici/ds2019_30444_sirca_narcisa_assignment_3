package com.example.springdemo.services;

import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.dto.builders.UserBuilder;
import com.example.springdemo.entities.User;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.UserRepository;
import com.example.springdemo.validators.UserFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {


    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) { this.userRepository = userRepository; }

    public UserDTO findUserById(Integer id){
        Optional<User> user  = userRepository.findById(id);
        if (!user.isPresent()) {
            throw new ResourceNotFoundException("User", "user id", id);
        }
        return UserBuilder.generateDTOFromEntity(user.get());
    }
    public UserDTO findByUsername(String username){
        Optional<User> user  = Optional.ofNullable(userRepository.findByUsername(username)); //aici trebuie mai sus nu
        if (!user.isPresent()) {
            throw new ResourceNotFoundException("User", "username", username);
        }
        return UserBuilder.generateDTOFromEntity(user.get());
    }

    public List<UserDTO> findAll(){
        List<User> users = userRepository.findAll();
        return users.stream()
                .map(UserBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(UserDTO userDTO) {

        UserFieldValidator.validateInsertOrUpdate(userDTO);
        return userRepository
                .save(UserBuilder.generateEntityFromDTO(userDTO))
                .getId();
    }

    public Integer update(UserDTO userDTO) {

        Optional<User> user = userRepository.findById(userDTO.getId());
        if(!user.isPresent()){
            throw new ResourceNotFoundException("User", "user id", userDTO.getId().toString());
        }
        UserFieldValidator.validateInsertOrUpdate(userDTO);
        return userRepository.save(UserBuilder.generateEntityFromDTO(userDTO)).getId();
    }

    public void delete(UserDTO userViewDTO){
        this.userRepository.deleteById(userViewDTO.getId());
    }

}
