package com.example.springdemo.services;

import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.dto.builders.MedicationPlanBuilder;
import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {

    public final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository){
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public List<MedicationPlanDTO> findMedicationPlanByPatientId(Integer patientId){
        Optional<List<MedicationPlan>> medicationPlanlistO  = Optional.ofNullable(medicationPlanRepository.findByPatientId(patientId));

        if (!medicationPlanlistO.isPresent()) {
            throw new ResourceNotFoundException("MedicationPlan", "patient id", patientId);
        }
        List<MedicationPlan> medicationPlanlist = medicationPlanlistO.get();

        return medicationPlanlist.stream()
                .map(x->MedicationPlanBuilder.generateDTOFromEntity(x))
                .collect(Collectors.toList());
    }
}