package com.example.springdemo.grpc;

import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.entities.TakenMedicine;
import com.example.springdemo.repositories.TakenMedicineRepository;
import com.example.springdemo.services.MedicationPlanService;
import grpc.pillbox.*;
import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.grpc.stub.StreamObserver;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@GRpcService
public class MedicationPlanServiceImpl
        extends MedicationPlanServiceGrpc.MedicationPlanServiceImplBase {

    private final MedicationPlanService medicationPlanService;

    private final TakenMedicineRepository takenMedicineRepository;

    private static final Logger LOGGER =
            LoggerFactory.getLogger(MedicationPlanServiceImpl.class);

    public MedicationPlanServiceImpl(MedicationPlanService medicationPlanService, TakenMedicineRepository takenMedicineRepository) {
        this.medicationPlanService = medicationPlanService;
        this.takenMedicineRepository = takenMedicineRepository;
    }

    @Override
    public void getMedicationPlans(MedicationPlanListRequest request,
                      StreamObserver<MedicationPlanListResponse> responseObserver) {
        LOGGER.info("server received {}", request);

        String message = "Am primit requestul: "+ request.getRequestMsg() + "de pa patient id: " + request.getPatientId();
        System.out.println(message);

        List<MedicationPlanDTO> medicationPlanList =
                medicationPlanService.findMedicationPlanByPatientId(request.getPatientId());

         List<grpc.pillbox.MedicationPlanResponse.Builder>  medicationPlanBuilderList = new ArrayList<>();


         for(MedicationPlanDTO mp: medicationPlanList ) {
             medicationPlanBuilderList.add(grpc.pillbox.MedicationPlanResponse.newBuilder()
             .setMedicationPlanId(mp.getMedicationPlanId())
             .setStartDate(mp.getStartDate().toString())
             .setEndDate(mp.getEndDate().toString())
             .setIntakeIntervalsAll(mp.getIntakeIntervalsAll()));
         }

        Iterable<grpc.pillbox.MedicationPlanResponse> medicationPlanResponseList = medicationPlanBuilderList.stream().map(x->x.build()).collect(Collectors.toList());
        MedicationPlanListResponse response = MedicationPlanListResponse.newBuilder().addAllMedicationPlanList(medicationPlanResponseList).build();
        LOGGER.info("server responded {}", response);

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void isMedicationTaken(TakenMedicineRequest request,
                                  StreamObserver<TakenMedicineResponse> responseObserver) {
        LOGGER.info("server received {}", request);

        String message = "Am primit requestul: "+ request.getIsTaken()
                + " medicamentul: " + request.getMedication()
                + " de pa patient id: " + request.getPatientId();

        System.out.println(message);

        String[] parseName = request.getMedication().split(" ");
        //save taken medicine info ito db
        takenMedicineRepository.save(new TakenMedicine(
                request.getPatientId(),
                parseName[1],
                request.getIsTaken()
        ));

        grpc.pillbox.TakenMedicineResponse response =
                grpc.pillbox.TakenMedicineResponse.newBuilder()
                .setServerConfirmation("Server confirmation")
                .build();

        LOGGER.info("server responded {}", response);
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
