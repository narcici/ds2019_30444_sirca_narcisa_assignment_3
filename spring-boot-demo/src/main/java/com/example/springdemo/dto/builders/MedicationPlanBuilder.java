package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.entities.MedicationPlan;

import java.util.Date;

public class MedicationPlanBuilder {


    public static MedicationPlanDTO generateDTOFromEntity(MedicationPlan medicationPlan){
        return new MedicationPlanDTO(
        medicationPlan.getMedicationPlanId(),
        medicationPlan.getStartDate(),
        medicationPlan.getEndDate(),
        medicationPlan.getIntakeIntervalsAll());
    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanDTO medicationPlanDTO){
        return new MedicationPlan(
                medicationPlanDTO.getMedicationPlanId(),
                medicationPlanDTO.getStartDate(),
                medicationPlanDTO.getEndDate(),
                medicationPlanDTO.getIntakeIntervalsAll()
               );
    }
}
