package com.example.springdemo.dto;

import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DoctorWithPatientsDTO {

    private User user;
    private Integer doctorId;
    private String name;
    private List<PatientDTO> patients;

}
