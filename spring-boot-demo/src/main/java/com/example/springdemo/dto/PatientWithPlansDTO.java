package com.example.springdemo.dto;

import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Doctor;
import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.entities.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PatientWithPlansDTO {

    private User user;
    private Integer patientId;
    private String name;
    private String address;
    private  String birthdate;
    private Character gender;
    private String medicalrecord;
    // in plus
    private List<MedicationPlanDTO> medicationPlanList;

}
