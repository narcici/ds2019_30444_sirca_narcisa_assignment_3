package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.entities.Patient;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PatientBuilder {

    public static PatientDTO generateDTOFromEntity(Patient patient){
        return new PatientDTO(
                patient.getPatientid(),
                patient.getName(),
                patient.getAddress(),
                patient.getBirthdate(),
                patient.getGender(),
                patient.getMedicalrecord(),
                patient.getUser());
    }


    public static Patient generateEntityFromDTO(PatientDTO patientDTO){
        return new Patient(
                patientDTO.getUser(),
                patientDTO.getPatientId(),
                patientDTO.getName(),
                patientDTO.getBirthdate(),
                patientDTO.getGender(),
                patientDTO.getAddress(),
                patientDTO.getMedicalrecord());
    }
}
