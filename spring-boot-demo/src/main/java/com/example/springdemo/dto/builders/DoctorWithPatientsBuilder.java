package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.DoctorWithPatientsDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.entities.Doctor;
import com.example.springdemo.entities.Patient;

import java.util.List;
import java.util.stream.Collectors;

public class DoctorWithPatientsBuilder {

        private DoctorWithPatientsBuilder(){}

        public static DoctorWithPatientsDTO generateDTOFromEntity(Doctor doctor, List<Patient> patients){
            List<PatientDTO> dtos =  patients.stream()
                    .map(PatientBuilder::generateDTOFromEntity)
                    .collect(Collectors.toList());

            return new DoctorWithPatientsDTO(
                    doctor.getUser(),
                    doctor.getDoctorId(),
                    doctor.getName(),
                    dtos);
        }
    }
