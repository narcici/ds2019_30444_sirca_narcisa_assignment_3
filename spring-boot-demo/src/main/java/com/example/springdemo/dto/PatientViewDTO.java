package com.example.springdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PatientViewDTO {
    private Integer user;
    private  String birthdate;
    private Character gender;
    private Integer caregiver_user;

}
