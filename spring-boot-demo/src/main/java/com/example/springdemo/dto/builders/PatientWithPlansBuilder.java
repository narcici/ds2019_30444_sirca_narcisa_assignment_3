package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.dto.PatientWithPlansDTO;
import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.entities.Patient;

import java.util.List;
import java.util.stream.Collectors;

public class PatientWithPlansBuilder {
        private PatientWithPlansBuilder(){}

        public static PatientWithPlansDTO generateDTOFromEntity(Patient patient, List<MedicationPlan> medicationPlanList){
            List<MedicationPlanDTO> dtos =  medicationPlanList.stream()
                    .map(MedicationPlanBuilder::generateDTOFromEntity)
                    .collect(Collectors.toList());

            return new PatientWithPlansDTO(
            patient.getUser(),
            patient.getPatientid(),
            patient.getName(),
            patient.getAddress(),
            patient.getBirthdate(),
            patient.getGender(),
            patient.getMedicalrecord(),
             dtos);
        }

    }
