package com.example.springdemo.repositories;

import com.example.springdemo.entities.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {


    @Query(value = "SELECT p " +
            "FROM MedicationPlan p " +
            "inner join p.patient u " +
            "where u.patientId = ?1"
    )
    List<MedicationPlan> findByPatientId(int patientId);
}