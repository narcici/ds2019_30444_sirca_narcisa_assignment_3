package com.example.springdemo.repositories;

import com.example.springdemo.entities.TakenMedicine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TakenMedicineRepository extends JpaRepository<TakenMedicine, Integer> {
}
