package com.example.springdemo.repositories;


import com.example.springdemo.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer> {

    @Query(value = "SELECT p " +
            "FROM Patient p " +
            "INNER JOIN FETCH p.medicationPlanList i"
    )
    List<Patient> getAllFetch();

    @Query(value = "SELECT u " +
            "FROM Patient u " +
            "ORDER BY u.name")
    List<Patient> getAllOrdered();


    // returneaza lista de medication pe care le are
}
