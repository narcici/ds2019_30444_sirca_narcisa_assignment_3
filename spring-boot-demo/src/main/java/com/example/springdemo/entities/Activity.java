package com.example.springdemo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Entity
@Table(name = "activity")
public final class Activity implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "activity_id", unique = true, nullable = false)
    private Integer activityId;

    @Column(name = "id")
    private final int id; // of the patient

    @Column(name = "activity", length = 30)
    private final String activity;

    @Column(name = "start")
    private final Date start;

    @Column(name = "end")
    private final Date end;

    public Activity(@JsonProperty("id") int id,
                    @JsonProperty("activity") String activity,
                    @JsonProperty("start") Date start,
                    @JsonProperty("end") Date end
    ) {
        this.id = id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public Activity(Integer activityId, int id,String activity,Date start,Date end) {
        this.activityId = activityId;
        this.id = id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id='" + id + "'" +
                ", activity=" + activity +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}