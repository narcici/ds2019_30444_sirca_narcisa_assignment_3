package com.example.springdemo.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "doctor")
public class Doctor {

    @OneToOne
    @JoinColumn(name="userid")
    private User user;

    @Id
    private Integer doctorId;

    @Column(name = "name", length = 100)
    private String name;

    @OneToMany(mappedBy="doctor",fetch = FetchType.LAZY)
    private List<Patient> patients = new ArrayList<Patient>();

    public Doctor() {
    }
    public Doctor(User user, Integer doctorId, String name ) {

        this.user = user;
        this.doctorId = doctorId;
        this.name = name;
    }

    public User getUser() {   return user;   }

    public void setUser(User user) { this.user = user;    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Patient> getPatients() {  return patients;  }

    public void setPatients(List<Patient> patients) {   this.patients = patients;  }
}