package com.example.springdemo.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "caregiver")
public class Caregiver {

    @OneToOne
    @JoinColumn(name="userid")
    private User user;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "caregiver_id", unique = true, nullable = false)
    private Integer caregiverId;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name="birthdate", length=45)
    private String birthdate;

    @Column(name="gender")
    private Character gender; // F M

    @Column(name="address", length=100)
    private String address;

    @OneToMany(mappedBy="caregiver", fetch = FetchType.LAZY)
    private List<Patient> patients = new ArrayList<Patient>();

    public Caregiver() {
    }

    public Caregiver(User user, Integer caregiverId, String name, String birthdate, String address, Character gender){
        this.user = user;
        this.caregiverId = caregiverId;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
    }

    public Integer getCaregiverId() {
        return caregiverId;
    }

    public void setCaregiverId(Integer caregiverId) {
        this.caregiverId = caregiverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() { return birthdate; }

    public void setBirthdate(String birthdate) { this.birthdate = birthdate; }

    public Character getGender() {  return gender; }

    public void setGender(Character gender) {  this.gender = gender; }

    public String getAddress() {     return address;  }

    public void setAddress(String address) {  this.address = address; }

    public User getUser() {   return user; }

    public void setUser(User user) {  this.user = user; }

    public List<Patient> getPatients() { return patients; }

    public void setPatients(List<Patient> patients) {  this.patients = patients; }

}
