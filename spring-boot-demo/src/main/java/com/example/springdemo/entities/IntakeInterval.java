package com.example.springdemo.entities;

import javax.persistence.*;

@Entity
@Table(name="intake_interval")
public class IntakeInterval {

    @Id
    private Integer intakeIntervalId;

    @ManyToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "medication_planId")
    private MedicationPlan medicationPlan;

    @Column(name = "morning")
    private Byte morning;

    @Column(name = "noon")
    private Byte noon;

    @Column(name = "evening")
    private Byte evening;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "medication_id")
    private Medication medication;

    public IntakeInterval() { }

    public IntakeInterval(Integer intakeIntervalId, MedicationPlan medicationPlan, Medication medication) {
        this.intakeIntervalId = intakeIntervalId;
        this.medicationPlan = medicationPlan;
        this.medication = medication;
    }

    public Integer getIntakeIntervalId() {
        return intakeIntervalId;
    }

    public void setIntakeIntervalId(Integer intakeIntervalId) {
        this.intakeIntervalId = intakeIntervalId;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    public Byte getMorning() {
        return morning;
    }

    public void setMorning(Byte morning) {
        this.morning = morning;
    }

    public Byte getNoon() {
        return noon;
    }

    public void setNoon(Byte noon) {
        this.noon = noon;
    }

    public Byte getEvening() {
        return evening;
    }

    public void setEvening(Byte evening) {
        this.evening = evening;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }
}
