package com.example.springdemo.controller;

import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.PatientWithPlansDTO;
import com.example.springdemo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping()
    public List<PatientDTO> findAllPatients(){
        return patientService.findAll();
    }

    @PostMapping()
    public Integer insertPatientDTO(@RequestBody PatientDTO patientDTO){
        return patientService.insert(patientDTO);
    }

    @GetMapping(value = "/{id}")
    public PatientWithPlansDTO findPatientById(@PathVariable("id") Integer id) {
        return patientService.findPatientById(id);
    }

    @GetMapping(value = "/byuserid/{id}")
    public PatientWithPlansDTO findPatientByUserId(@PathVariable("id") Integer id) {
        return patientService.findPatientByUserid(id);
    }

    @PutMapping()
    public Integer updatePatient(@RequestBody PatientDTO patientDTO) {
        return patientService.update(patientDTO);
    }

    @DeleteMapping()
    public Integer deletePatient(@RequestBody PatientDTO patientDTO){
       return  patientService.delete(patientDTO);
    }

}
