package com.example.pillbox.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class MedicationClient {

    private String MedicationName;
    private Date startHour;
    private Date endHour;

    public MedicationClient(String medicationName, Date startHour, Date endHour) {
        MedicationName = medicationName;
        this.startHour = startHour;
        this.endHour = endHour;
    }
}
