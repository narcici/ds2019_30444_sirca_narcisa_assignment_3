package com.example.pillbox.view;

import com.example.pillbox.entity.MedicationClient;
import grpc.pillbox.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

@Getter
@Setter
public class PillboxView extends Frame implements Runnable {

    private JLabel currentTime;
    private JPanel mainPanel;

    private Date dd;
    private Thread th;

    //keep the info about the starting minute of this application
    private int startingMinute;
    //var counting how many medications are left to be taken
    private int nrOfMedicationsToTake;

    //creez un request pe care sa il trimit
//    TakenMedicineRequest request = TakenMedicineRequest.newBuilder()
//                                .setIsTaken("NOT TAKEN !")
//                                .setMedication("Taken Medicine")
//                                .setPatientId(1)
//                                .build();

    public PillboxView() {

        setVisible(true);
        setSize(1000,1000);

        // initially no meds to be taken
        nrOfMedicationsToTake = 0;

        mainPanel = new JPanel();
        mainPanel.setBounds( 100,120, 800,600);
        mainPanel.setBackground(Color.lightGray);
        mainPanel.setVisible(true);
        this.add(mainPanel);

        currentTime = new JLabel("00:00:00");
        add(currentTime);
        currentTime.setHorizontalAlignment(JLabel.CENTER);
        currentTime.setVerticalAlignment(JLabel.NORTH);

        Font font = new Font(null, Font.PLAIN, 35);
        currentTime.setFont(font);

        startingMinute = new Date().getMinutes(); //saving the minute in which the app started to = this view was created
        System.out.println("ctor: strartingMinute: " + startingMinute);
        th = new Thread(this);
        th.start(); // here thread starts
    }

    @Override
    public void run() {
        {
            try {
                do {
                    dd = new Date();
                    currentTime.setText(dd.getHours() + " : " + dd.getMinutes() + " : " + dd.getSeconds());
                    Thread.sleep(1000);  // 1000 = 1 second
                } while (th.isAlive());
            } catch (Exception e) {
                System.out.println("catch exception" + e.getClass());
            }
        }
    }

    public void addMedicationPanel(MedicationClient medicationClient, int x){

        nrOfMedicationsToTake++; // one more medication to be taken
        System.out.println("add panel: meds to take: " + nrOfMedicationsToTake);
        Panel panel = new Panel();
        panel.setBounds(x, 50, 700,40);
        //panel.setSize(400,30);
        panel.setLayout(new FlowLayout());
        panel.setBackground(Color.gray);

        Font font = new Font(null, Font.PLAIN, 35);
        JLabel m = new JLabel(medicationClient.getMedicationName());
        m.setFont(font);
        panel.add(m);
        m.setBackground(Color.white);
        panel.add(new JLabel(medicationClient.getStartHour().toString()));
        panel.add(new JLabel(medicationClient.getEndHour().toString()));
        Button b = new Button("Take "+ medicationClient.getMedicationName());
        b.setBackground(Color.lightGray);
        b.addActionListener( e ->
        {
            nrOfMedicationsToTake--; // one less medication to be taken
            System.out.println("button press: meds to take: " + nrOfMedicationsToTake);

            ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 6565)
                    .usePlaintext()
                    .build();

            MedicationPlanServiceGrpc.MedicationPlanServiceBlockingStub stub
                    = MedicationPlanServiceGrpc.newBlockingStub(channel);

            String takenMedicine =b.getLabel();
            System.out.println(takenMedicine);

            TakenMedicineResponse response = stub.isMedicationTaken(TakenMedicineRequest.newBuilder()
                    .setIsTaken("TAKEN")
                    .setMedication(takenMedicine)
                    .setPatientId(1)
                    .build());

            System.out.println(response.getServerConfirmation());

            channel.shutdown();
            panel.setVisible(false);
        });
        panel.setVisible(true);
        panel.add(b);
        mainPanel.add(panel);
    }
}