package com.example.pillbox.controller;

import com.example.pillbox.entity.MedicationClient;
import com.example.pillbox.view.PillboxView;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Setter
@Getter
public class PillboxController  {

    private PillboxView pillboxView;
    List<MedicationClient> medicationList = new ArrayList<>();

    public PillboxController(PillboxView pillboxView){
        this.pillboxView = pillboxView;
    }

    public void addMedication(MedicationClient medication){
        medicationList.add(medication);
    }

    public void addMedicationPanels(){
        if(!medicationList.isEmpty()) {
            int x = 50;
            for(MedicationClient medicationClient:medicationList) {
                pillboxView.addMedicationPanel(medicationClient, x);
                x += 30;
            }
        }
        else
            JOptionPane.showMessageDialog(null, " You don't have medicines for now!!");
    }

}
